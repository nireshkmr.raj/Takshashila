package com.codeworks.project.materialdesigns.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.codeworks.project.materialdesigns.MenuActivity;
import com.codeworks.project.materialdesigns.R;
import com.codeworks.project.materialdesigns.ResideMenu.ResideMenu;
import com.codeworks.project.materialdesigns.springylib.SpringAnimationType;
import com.codeworks.project.materialdesigns.springylib.SpringyAnimator;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private View parentView;
    private ResideMenu resideMenu;
    private ImageView technical,cultural,workshop;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.home, container, false);
        setUpViews();
        return parentView;
    }

    private void setUpViews() {
        MenuActivity parentActivity = (MenuActivity) getActivity();
        resideMenu = parentActivity.getResideMenu();
        technical=parentView.findViewById(R.id.tech_imageview);
        cultural=parentView.findViewById(R.id.cultural_imageview);
        workshop=parentView.findViewById(R.id.workshop_imageview);
        technical.setOnClickListener(this);
        cultural.setOnClickListener(this);
        workshop.setOnClickListener(this);
        final SpringyAnimator scaleY = new SpringyAnimator(SpringAnimationType.SCALEY,5,3,0.5f,1);
        final SpringyAnimator rotate = new SpringyAnimator(SpringAnimationType.ROTATEY,5,3,180,0);
        rotate.setDelay(200);
        scaleY.setDelay(300);
        rotate.startSpring(technical);
        scaleY.startSpring(technical);

        rotate.startSpring(cultural);
        scaleY.startSpring(cultural);

        rotate.startSpring(workshop);
        scaleY.startSpring(workshop);

     /*   parentView.findViewById(R.id.btn_open_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });

        // add gesture operation's ignored views
        FrameLayout ignored_view = (FrameLayout) parentView.findViewById(R.id.ignored_view);
        resideMenu.addIgnoredView(ignored_view);
        */
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tech_imageview:
                ((MenuActivity)getActivity()).setUpMenu("tech",1);
                Toast.makeText(getActivity(), "technical", Toast.LENGTH_SHORT).show();
                break;
            case R.id.cultural_imageview:
                Toast.makeText(getActivity(), "culturals", Toast.LENGTH_SHORT).show();
                ((MenuActivity)getActivity()).setUpMenu("cultural",1);
                break;
            case R.id.workshop_imageview:
                Toast.makeText(getActivity(), "workshop", Toast.LENGTH_SHORT).show();
                ((MenuActivity)getActivity()).setUpMenu("workshop",1);
                break;
        }
    }
}
