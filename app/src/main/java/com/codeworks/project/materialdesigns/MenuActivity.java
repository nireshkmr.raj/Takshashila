package com.codeworks.project.materialdesigns;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import com.codeworks.project.materialdesigns.ResideMenu.ResideMenu;
import com.codeworks.project.materialdesigns.ResideMenu.ResideMenuItem;
import com.codeworks.project.materialdesigns.fragment.CalendarFragment;
import com.codeworks.project.materialdesigns.fragment.HomeFragment;
import com.codeworks.project.materialdesigns.fragment.ProfileFragment;
import com.codeworks.project.materialdesigns.fragment.SettingsFragment;

import java.util.ArrayList;


public class MenuActivity extends FragmentActivity implements View.OnClickListener{
    private ArrayList<Integer> images=new ArrayList<>();
    private ArrayList<String> names=new ArrayList<>();
    private ResideMenu resideMenu;
    private MenuActivity mContext;
    private ResideMenuItem itemHome;
    private ResideMenuItem itemProfile;
    private ResideMenuItem itemCalendar;
    private ResideMenuItem itemSettings;
    private ResideMenuItem dance,music,paintertainment,nerdy,photography,theatre,literary_eng,literary_tamil,variety,gaming,sports,lifestyle,sherlock,online,fashions;
    private ResideMenuItem coding,mreng,circuit,robotics,buildies,minds;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mContext = this;
        ImageView imageView=findViewById(R.id.backOption);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setUpMenu("workshop",0);
        if( savedInstanceState == null )
            changeFragment(new HomeFragment());
    }

    public void setUpMenu(String s,int x) {
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(false);
       // resideMenu.setBackground(R.drawable.techicon);
        resideMenu.attachToActivity(this);
        //resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        // create menu items;

        if (s.equals("tech")) {
            coding     = new ResideMenuItem(this, R.drawable.coding,     "CODING");
            mreng  = new ResideMenuItem(this, R.drawable.mrengg,  "MR.ENGINEERING");
            robotics = new ResideMenuItem(this, R.drawable.  roboticscircle, "ROBOTICS");
            circuit = new ResideMenuItem(this, R.drawable.circuitscircle, "CIRCUIT");
            buildies = new ResideMenuItem(this, R.drawable.buildiescircle, "BUILDIES");
            minds = new ResideMenuItem(this, R.drawable.mindscircle, "MINDS");
            resideMenu.setBackground(R.drawable.techicon);

            coding.setOnClickListener(this);
            mreng.setOnClickListener(this);
            robotics.setOnClickListener(this);
            circuit.setOnClickListener(this);
            buildies.setOnClickListener(this);
            minds.setOnClickListener(this);

            resideMenu.addMenuItem(coding, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(mreng, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(robotics, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(circuit, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(buildies, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(minds, ResideMenu.DIRECTION_LEFT);

            // You can disable a direction by setting ->
            resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
            resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
            if (x==1)
            resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);

        }else if(s.equals("cultural")){
            resideMenu.setBackground(R.drawable.nontechicon);
            dance     = new ResideMenuItem(this, R.drawable.dancecategory,     "DANCE");
            music  = new ResideMenuItem(this, R.drawable.music,  "MUSIC");
            paintertainment = new ResideMenuItem(this, R.drawable.  paintertainment, "PAINTERTAINMENT");
            nerdy = new ResideMenuItem(this, R.drawable.nerdy, "NERDY");
            photography = new ResideMenuItem(this, R.drawable.photography, "PHOTOGRAPHY");
            theatre = new ResideMenuItem(this, R.drawable.theatrecircle, "THEATRE");
            literary_eng = new ResideMenuItem(this, R.drawable.literaryenglish, "LITERARY ENGLISH");
            literary_tamil = new ResideMenuItem(this, R.drawable.tamil, "LITERARY TAMIL");
            variety = new ResideMenuItem(this, R.drawable.variety, "VARIETY");
            gaming = new ResideMenuItem(this, R.drawable.gaming, "GAMING");
            sports = new ResideMenuItem(this, R.drawable.sports, "SPORTS");
            lifestyle = new ResideMenuItem(this, R.drawable.lifestyle, "LIFESTYLE");
            sherlock = new ResideMenuItem(this, R.drawable.sherlock, "SHERLOCK");
            online = new ResideMenuItem(this, R.drawable.online, "ONLINE EVENTS");
            fashions = new ResideMenuItem(this, R.drawable.fashions, "FASHIONZ");


            dance.setOnClickListener(this);
            music.setOnClickListener(this);
            paintertainment.setOnClickListener(this);
            nerdy.setOnClickListener(this);
            photography.setOnClickListener(this);
            theatre.setOnClickListener(this);
            literary_tamil.setOnClickListener(this);
            literary_eng.setOnClickListener(this);
            variety.setOnClickListener(this);
            gaming.setOnClickListener(this);
            sports.setOnClickListener(this);
            lifestyle.setOnClickListener(this);
            sherlock.setOnClickListener(this);
            online.setOnClickListener(this);
            fashions.setOnClickListener(this);

            resideMenu.addMenuItem(dance, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(music, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(paintertainment, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(nerdy, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(photography, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(theatre, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(literary_eng, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(literary_tamil, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(variety, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(gaming, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(sports, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(lifestyle, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(sherlock, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(online, ResideMenu.DIRECTION_RIGHT);
            resideMenu.addMenuItem(fashions, ResideMenu.DIRECTION_RIGHT);
            // You can disable a direction by setting ->
            resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
            resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
            if (x==1)
            resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);

        }else if(s.equals("workshop")){
            resideMenu.setBackground(R.drawable.workshop);
            itemHome     = new ResideMenuItem(this, R.drawable.coding,     "CODING");
            itemProfile  = new ResideMenuItem(this, R.drawable.dancecategory,  "DANCE");
            itemCalendar = new ResideMenuItem(this, R.drawable.music, "MUSIC");
            itemSettings = new ResideMenuItem(this, R.drawable.sherlock, "SHERLOCK");

            itemHome.setOnClickListener(this);
            itemProfile.setOnClickListener(this);
            itemCalendar.setOnClickListener(this);
            itemSettings.setOnClickListener(this);

            resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(itemProfile, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(itemCalendar, ResideMenu.DIRECTION_LEFT);
            resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);

            // You can disable a direction by setting ->
            resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
            resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
            if (x==1)
            resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
        }
            // attach to current activity;

        /*findViewById(R.id.title_bar_right_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
            }
        });*/
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if (view == itemHome){
            changeFragment(new HomeFragment());
        }else if (view == itemProfile){
            changeFragment(new ProfileFragment());
        }else if (view == itemCalendar){
            changeFragment(new CalendarFragment());
        }else if (view == itemSettings){
            changeFragment(new SettingsFragment());
        }

        resideMenu.closeMenu();
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            Toast.makeText(mContext, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            Toast.makeText(mContext, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void changeFragment(Fragment targetFragment){
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access resideMenu？
    public ResideMenu getResideMenu(){
        return resideMenu;
    }
}
