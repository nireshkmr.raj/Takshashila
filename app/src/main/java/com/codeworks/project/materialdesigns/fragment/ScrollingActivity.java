package com.codeworks.project.materialdesigns.fragment;

/**
 * Created by Niru.R on 12-19-2017.
 */

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.codeworks.project.materialdesigns.R;

import java.util.ArrayList;

public class ScrollingActivity extends AppCompatActivity {

    private int choice;
    private String title,description,timings,rules,coordinator,call_number;
    private ArrayList<String> coordinator_list,call_list;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_description);
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        CollapsingToolbarLayout  cToolbar=findViewById(R.id.toolbar_layout);
        setSupportActionBar(mToolbar);
      /*  choice=getIntent().getExtras().getInt("choice");
        if (choice==0){
            title=getIntent().getExtras().getString("title");
//            description=getIntent().getExtras().getString("description");
//            timings=getIntent().getExtras().getString("timings");
//            rules=getIntent().getExtras().getString("rules");
//            coordinator=getIntent().getExtras().getString("coordinator");
//            call_number=getIntent().getExtras().getString("call");
        }else{
            title=getIntent().getExtras().getString("title");
//            description=getIntent().getExtras().getString("description");
//            timings=getIntent().getExtras().getString("timings");
//            rules=getIntent().getExtras().getString("rules");
//            coordinator_list=getIntent().getExtras().getStringArrayList("coordinators");
//            call_list=getIntent().getExtras().getStringArrayList("calls");
        }
        mToolbar.setTitle(title);
        cToolbar.setTitle(title);
        */
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if ((scrollRange + verticalOffset) <= 170&&(scrollRange + verticalOffset)>=0) {
                    isShow = true;
                    showOption(R.id.action_info);
                } else if (isShow) {
                    isShow = false;
                    hideOption(R.id.action_info);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        hideOption(R.id.action_info);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_info) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hideOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }
}