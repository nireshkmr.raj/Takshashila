package com.codeworks.project.materialdesigns.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.codeworks.project.materialdesigns.R;

public class HomePage extends Fragment {
  CardView about_us,highlights,events;
    TextView title,content,higlights_t;
    Animation animation;


    public HomePage() {
        // Required empty public constructor
    }

    public static HomePage newInstance() {
        HomePage fragment = new HomePage();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        about_us=view.findViewById(R.id.aboutus);
        //title=view.findViewById(R.id.head);
        content=view.findViewById(R.id.desc);
        higlights_t=view.findViewById(R.id.highlights);

        content.setText(getResources().getString(R.string.title_content));
        higlights_t.setAnimation(AnimationUtils.loadAnimation(getActivity(),R.anim.blink));
        //title.setAnimation(AnimationUtils.loadAnimation(getActivity(),R.anim.move));
        content.setAnimation(AnimationUtils.loadAnimation(getActivity(),R.anim.slide_down));



    }
}
