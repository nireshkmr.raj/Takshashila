package com.codeworks.project.materialdesigns;

import android.support.v7.widget.Toolbar;

/**
 * Created by Niru.R on 12-17-2017.
 */

public interface Interaction {
    void toolbar(Toolbar toolbar);

}
