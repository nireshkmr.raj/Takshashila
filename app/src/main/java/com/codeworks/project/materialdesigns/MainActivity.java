package com.codeworks.project.materialdesigns;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.codeworks.project.materialdesigns.fragment.HomePage;
import com.codeworks.project.materialdesigns.fragment.MainFragment;
import com.codeworks.project.materialdesigns.fragment.QrFragment;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.widget.FButton;

public class MainActivity extends AppCompatActivity implements Interaction,Toggle{
    FragmentManager fm;
    @BindView(R.id.vNavigation)
    NavigationView vNavigation;
    @BindView(R.id.drawerlayout)
    FlowingDrawer mDrawer;
     Toolbar toolbar;
      FButton qrCodeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle("TAKSHASHILA");
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        fm = getSupportFragmentManager();
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        View header=vNavigation.getHeaderView(0);
        qrCodeButton=header.findViewById(R.id.qrcodebutton);
        setUpFront();
        qrCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setToolbar();
                mDrawer.closeMenu();
                QrFragment fragment=new QrFragment();
                fragment.setToggle(MainActivity.this);
                changeFragment(fm,fragment);
            }
        });
    }

    public void setUpFront(){
        setSupportActionBar(null);
        MainFragment fragment=new MainFragment();
        fragment.setToolbar(MainActivity.this);
        changeFragment(fm,fragment);
        setupMenu();
    }

    public void setToolbar(){
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Profile");
        toolbar.setNavigationIcon(R.drawable.ic_menu_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.toggleMenu();
            }
        });
    }

    private void setupMenu() {
        vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.mainhome:
                      setUpFront();
                        break;
                    case R.id.profile:
                        setToolbar();
                        HomePage page=new HomePage();
                        changeFragment(fm,page);
                        break;
                    case R.id.events:
                        startActivity(new Intent(MainActivity.this,MenuActivity.class));
                        break;
                    case R.id.gallery:
                        break;
                    case R.id.sponsors:
                        break;
                    case R.id.cause:
                        break;
                    case R.id.schedule:
                        break;
                    case R.id.tickets:
                        break;
                    case R.id.hospitality:
                        break;
                }
                mDrawer.closeMenu();
                return false;
            }
        }) ;
    }
    private void changeFragment(FragmentManager fm,Fragment fragment) {

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.containerlayout, fragment).commit();
        }else{
            RelativeLayout layout=findViewById(R.id.containerlayout);
            layout.removeAllViews();
        }
    }
        @Override
    public void onBackPressed() {
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public void toolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.toggleMenu();
            }
        });
    }

    @Override
    public void toggle() {
        mDrawer.toggleMenu();
    }
}
