package com.codeworks.project.materialdesigns;

/**
 * Created by Niru.R on 12-17-2017.
 */

public interface Toggle {
    void toggle();
}
