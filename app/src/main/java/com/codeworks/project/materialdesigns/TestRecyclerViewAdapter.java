package com.codeworks.project.materialdesigns;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class TestRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    TextView textView;
    List<Object> contents;
    Context context;
    CardView cardView;
    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;
    ImageView imageView;
    public TestRecyclerViewAdapter(Context context,List<Object> contents) {
        this.contents = contents;
        this.context=context;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return TYPE_CELL;
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        switch (viewType) {
            case TYPE_HEADER: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_card_big, parent, false);
                textView=view.findViewById(R.id.desc);
                cardView=view.findViewById(R.id.card_view);
                imageView=view.findViewById(R.id.bgimage);
                return new RecyclerView.ViewHolder(view) {
                };
            }
            case TYPE_CELL: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_card_small, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
            }
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                textView.setText(context.getResources().getString(R.string.content));
                textView.setAnimation(AnimationUtils.loadAnimation(context,R.anim.slide_down));
                break;
            case TYPE_CELL:
                break;
        }
    }
}