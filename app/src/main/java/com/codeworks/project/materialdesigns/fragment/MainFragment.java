package com.codeworks.project.materialdesigns.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.codeworks.project.materialdesigns.Interaction;
import com.codeworks.project.materialdesigns.MainActivity;
import com.codeworks.project.materialdesigns.R;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends Fragment{

    MaterialViewPager mViewPager;
    public ImageView headerImg;
    public MainFragment() {
        // Required empty public constructor
    }
Interaction interaction=null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }
    public void setToolbar(Interaction interaction){
        this.interaction=interaction;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewPager=view.findViewById(R.id.materialViewPager);
        headerImg=view.findViewById(R.id.headerlogo);
        Toolbar toolbar = mViewPager.getToolbar();
        if (toolbar != null) {
            if (interaction!=null){
                interaction.toolbar(toolbar);
            }
        }
        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return RecyclerViewFragment.newInstance();
                    case 1:
                        return RecyclerViewFragment.newInstance();
                    case 2:
                        return ScrollFragment.newInstance();
                    default:
                        return RecyclerViewFragment.newInstance();
                }
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return "About Us";
                    case 1:
                        return "ProShows";
                    case 2:
                        return "Professionnel";
                }
                return "";
            }
        });

        mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
            @Override
            public HeaderDesign getHeaderDesign(int page) {
                switch (page) {
                    case 0:
                        //headerImg.setImageResource(R.drawable.logo_1);
                        return HeaderDesign.fromColorResAndDrawable(R.color.red,getResources().getDrawable(R.drawable.black_min));
                    case 2:
                        //headerImg.setImageResource(R.drawable.logo_1);
                        return HeaderDesign.fromColorResAndDrawable(R.color.cyan,getResources().getDrawable(R.mipmap.original));
                    case 1:
                        //headerImg.setImageResource(R.drawable.logo_1);
                        return HeaderDesign.fromColorResAndDrawable(R.color.blue,getResources().getDrawable(R.drawable.lillipop));
                }
                //execute others actions if needed (ex : modify your header logo)
                return null;
            }
        });

        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
        final View logo = view.findViewById(R.id.logo_white);
        if (logo != null) {
            logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewPager.notifyHeaderChanged();
                    Toast.makeText(getActivity(), "Yes, the title is clickable", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
