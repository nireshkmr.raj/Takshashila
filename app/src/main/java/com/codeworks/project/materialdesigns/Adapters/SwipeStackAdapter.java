package com.codeworks.project.materialdesigns.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.codeworks.project.materialdesigns.R;

import java.util.List;

/**
 * Created by Niru.R on 12-20-2017.
 */

public class SwipeStackAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    private List<Integer> mData;

    public SwipeStackAdapter(Context context, List<Integer> data) {
        this.mData = data;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return this.mData.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = this.inflater.inflate(R.layout.stackcard, parent, false);
        ((ImageView) convertView.findViewById(R.id.imgView)).setImageResource(((Integer) this.mData.get(position)).intValue());
        return convertView;
    }
}
