package com.codeworks.project.materialdesigns.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeworks.project.materialdesigns.R;

/**
 * User: special
 * Date: 13-12-22
 * Time: 下午1:31
 * Mail: specialcyci@gmail.com
 */
public class ProfileFragment extends Fragment {

    private static String eventName="name";
    private static String eventCoordinator="coordinator";
    private static String eventRules="rules";
    private static String eventDescription="description";
    private static String imageId="image";
    private View view;
    private TextView name,coordinator,description,rules;
    private ImageView image;
    public static ProfileFragment newInstance(int image_id,String event_name, String event_description,String event_rules,String event_coordinator) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(eventName, event_name);
        args.putString(eventDescription, event_description);
        args.putString(eventRules, event_rules);
        args.putString(eventCoordinator, event_coordinator);
        args.putInt(imageId,image_id);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.profile, container, false);
        name=view.findViewById(R.id.event_name);
        description=view.findViewById(R.id.event_description);
        rules=view.findViewById(R.id.event_rules);
        coordinator=view.findViewById(R.id.event_coordinator);
        image=view.findViewById(R.id.event_image);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        name.setText(getArguments().getString(eventName));
        description.setText(getArguments().getString(eventDescription));
        coordinator.setText(getArguments().getString(eventCoordinator));
        rules.setText(getArguments().getString(eventRules));
        image.setImageResource(getArguments().getInt(imageId));
    }
}
